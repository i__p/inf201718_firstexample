package com.cgm.academy.firstProject.car.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Car {
    private Integer id;
    private String regNo;
    private String brand;
    private String model;
}

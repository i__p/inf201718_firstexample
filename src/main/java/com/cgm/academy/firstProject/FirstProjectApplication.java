package com.cgm.academy.firstProject;

import com.cgm.academy.firstProject.car.dao.impl.CarJDBCDAO;
import com.cgm.academy.firstProject.car.model.Car;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstProjectApplication implements CommandLineRunner {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final CarJDBCDAO carDAO;

    @Autowired
    public FirstProjectApplication(CarJDBCDAO carDAO) {
        this.carDAO = carDAO;
    }

    public static void main(String[] args) {
        SpringApplication.run(FirstProjectApplication.class, args);
    }

    @Override
    public void run(String... args) {
        Car c1 = new Car(1, "LU00112", "Mercedes", "Vito");
        carDAO.insert(c1);
        logger.info("Car with id = 1: {}", carDAO.findById(1));
    }

}
